<?php

namespace App\Http\Controllers;

use App\Exceptions\ProductInventoryExceededException;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderResource;
use App\Repositories\ProductRepository;
use App\Services\OrderService;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function __construct(
        private OrderService $orderService,
        private ProductRepository $products
    ) {
    }

    public function store(StoreOrderRequest $request)
    {
        try {
            $order = $this->orderService->create(
                $this->products->findOrFail($request->getProductId()),
                $request->getQuantity()
            );

            return new OrderResource($order);
        } catch (ProductInventoryExceededException $ex) {
            return response([
                'errors' => ['quantity' => $ex->getMessage()]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
