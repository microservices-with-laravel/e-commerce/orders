<?php

namespace App\Console\Commands;

use App\Services\ProductService;
use App\Services\RedisService;
use Ecommerce\Common\Containers\Product\ProductContainer;
use Ecommerce\Common\Enums\Events;
use Illuminate\Console\Command;

class RedisConsumeCommand extends Command
{
    protected $signature = 'redis:consume';
    protected $description = 'Consume events from Redis stream';

    public function handle(
        RedisService $redis,
        ProductService $productService
    ) {
        foreach ($redis->getUnprocessedEvents() as $event) {
            match ($event['type']) {
                Events::PRODUCT_CREATED =>
                    $productService->create(
                        ProductContainer::fromArray($event['data'])
                    ),
                default => null
            };

            $redis->addProcessedEvent($event);
        }
    }
}
