<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    public function findOrFail(int $id): Product
    {
        return Product::findOrFail($id);
    }

    public function create(int $id, string $name, float $price): Product
    {
        return Product::create([
            'id' => $id,
            'name' => $name,
            'price' => $price,
        ]);
    }
}
