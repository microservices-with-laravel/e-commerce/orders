<?php

namespace App\Models;

use Ecommerce\Common\Containers\Order\OrderContainer;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function toContainer(): OrderContainer
    {
        return new OrderContainer(
            $this->product_id,
            $this->quantity,
            $this->total_price
        );
    }
}
