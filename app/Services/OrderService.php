<?php

namespace App\Services;

use App\Exceptions\ProductInventoryExceededException;
use App\Models\Order;
use App\Models\Product;
use App\Repositories\OrderRepository;

class OrderService
{
    public function __construct(
        private WarehouseService $warehouseService,
        private RedisService $redis,
        private OrderRepository $orders
    ) {
    }

    /**
     * @param Product $product
     * @param float $quantity
     * @return Order
     * @throws ProductInventoryExceededException|\Throwable
     */
    public function create(Product $product, float $quantity): Order
    {
        throw_if(
            $quantity > $this->warehouseService->getTotalInventory($product),
            new ProductInventoryExceededException("There is not enough $product->name in inventory")
        );

        $order = $this->orders->create($product, $quantity);
        $this->redis->publishOrderCreated($order->toContainer());

        return $order;
    }
}
