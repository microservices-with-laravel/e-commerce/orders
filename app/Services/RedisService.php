<?php

namespace App\Services;

use Ecommerce\Common\Containers\Order\OrderContainer;
use Ecommerce\Common\Events\Order\OrderCreatedEvent;
use Ecommerce\Common\Services\RedisService as BaseRedisService;

class RedisService extends BaseRedisService
{
    public function getServiceName(): string
    {
        return 'orders';
    }

    public function publishOrderCreated(OrderContainer $orderContainer): void
    {
        $this->publish(new OrderCreatedEvent($orderContainer));
    }
}
